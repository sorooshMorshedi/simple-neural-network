# Let’s create a simple neural network in Python. Neural networks are the building blocks of artificial intelligence.
# In this example, we’ll create a single neuron with three inputs and one output

import numpy as np

# Define the sigmoid activation function


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


# Initialize random weights for our single neuron
weights = np.random.rand(3)

# Input data (three features)
inputs = np.array([0.5, 0.3, 0.8])

# Calculate the weighted sum of inputs
weighted_sum = np.dot(inputs, weights)

# Apply the sigmoid function to get the output
output = sigmoid(weighted_sum)

# Print the result
print(f"Neuron output: {output:.4f}")
